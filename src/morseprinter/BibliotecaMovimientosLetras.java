package morseprinter;

/*
 * Contiene las instrucciones necesarias para dibujar cada caracter
 * */

public class BibliotecaMovimientosLetras {

	// Atributos
	private PlotStep plot; // impresora
	
	// Constructor
	public BibliotecaMovimientosLetras() throws Exception{

		plot = new PlotStep();
	} // fin del constructor
	
	// Imprime el caracter paso a paso, haciendo uso del PlotStep
	public void imprimir(String caracter) throws Exception{
		switch(caracter){
			case "A": //caracter = "A";
				plot.movimiento(75, 0, false);
				plot.movimiento(75, 100, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(0, 50, false);
				plot.movimiento(75, 50, true);
				plot.movimiento(85, 0, false);
				plot.finLetra();
				break;
			case "B": //caracter = "B";
				plot.movimiento(75, 50, false);
				plot.movimiento(75, 100, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(100, 50, true);
				plot.movimiento(0, 50, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "C": //caracter = "C";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "D": //caracter = "D";
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "E": //caracter = "E";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(50, 50, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "F":
				//caracter = "F";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(50, 50, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "G": //caracter = "G";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(100, 50, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(50, 25, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "H": //caracter = "H";
				plot.movimiento(100, 100, false);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(0, 50, false);
				plot.movimiento(100, 50, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "I": //caracter = "I";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(100, 0, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(50, 0, false);
				plot.movimiento(50, 100, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "J": //caracter = "J";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 100, false);
				plot.movimiento(50, 0, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "K": //caracter = "K";
				plot.movimiento(50, 100, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "L": // caracter = "L";
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "M": //caracter = "M";
				plot.movimiento(0, 50, true);
				plot.movimiento(50, 100, true);
				plot.movimiento(100, 50, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(100, 50, false);
				plot.movimiento(150, 100, true);
				plot.movimiento(200, 50, true);
				plot.movimiento(200, 0, true);
				plot.movimiento(210, 0, false);
				plot.finLetra();
				break;
			case "N": //caracter = "N";
				plot.movimiento(100, 100, false);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "O": //caracter = "O";
				plot.movimiento(100, 100, false);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(100, 100, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "P": //caracter = "P";
				plot.movimiento(0, 100, false);
				plot.movimiento(50, 100, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(0, 50, true);
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "Q": //caracter = "Q";
				plot.movimiento(100, 100, false);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(100, 100, true);
				plot.movimiento(100, 0, false);
				plot.movimiento(50, 50, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "R": //caracter = "R";
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 100, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(0, 50, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "S": //caracter = "S";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 50, true);
				plot.movimiento(100, 50, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "T": //caracter = "T";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 100, false);
				plot.movimiento(50, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "U": //caracter = "U";
				plot.movimiento(100, 100, false);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "V": //caracter = "V";
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(100, 50, true);
				plot.movimiento(100, 100, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "W": //caracter = "W";
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(100, 50, true);
				plot.movimiento(100, 100, true);
				plot.movimiento(100, 50, false);
				plot.movimiento(150, 0, true);
				plot.movimiento(200, 50, true);
				plot.movimiento(200, 100, true);
				plot.movimiento(210, 0, false);
				plot.finLetra();
				break;
			case "X": //caracter = "X";
				plot.movimiento(100, 100, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(0, 100, false);
				plot.movimiento(100, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "Y": //caracter = "Y";
				plot.movimiento(100, 100, false);
				plot.movimiento(50, 50, true);
				plot.movimiento(0, 100, false);
				plot.movimiento(50, 50, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "Z": //caracter = "Z";
				plot.movimiento(0, 100, false);
				plot.movimiento(100, 100, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			case "1": //caracter = "1";
				plot.movimiento(25, 0, false);
				plot.movimiento(25, 100, true);
				plot.movimiento(0, 75, true);
				plot.movimiento(35, 0, false);
				plot.finLetra();
				break;
			case "2": //caracter = "2";
				plot.movimiento(0, 100, false);
				plot.movimiento(50, 100, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(0, 50, true);
				plot.movimiento(0, 0, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "3":
				//caracter = "3";
				plot.movimiento(50, 0, true);
				plot.movimiento(50, 100, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(0, 50, false);
				plot.movimiento(50, 50, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "4":
				//caracter = "4";
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(50, 100, false);
				plot.movimiento(50, 0, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "5":
				//caracter = "5";
				plot.movimiento(50, 0, true);
				plot.movimiento(50, 50, true);
				plot.movimiento(0, 50, true);
				plot.movimiento(0, 100, true);						
				plot.movimiento(50, 100, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "6":
				//caracter = "6";
				plot.movimiento(0, 100, false);
				plot.movimiento(0, 0, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(50, 50, true);						
				plot.movimiento(0, 50, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "7":
				//caracter = "7";
				plot.movimiento(0, 75, false);
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 100, true);
				plot.movimiento(50, 0, true);						
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "8":
				//caracter = "8";
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 100, true);
				plot.movimiento(50, 0, true);
				plot.movimiento(0, 0, true);						
				plot.movimiento(0, 50, false);
				plot.movimiento(50, 50, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "9":
				//caracter = "9";
				plot.movimiento(50, 50, false);
				plot.movimiento(0, 50, true);
				plot.movimiento(0, 100, true);
				plot.movimiento(50, 100, true);						
				plot.movimiento(50, 0, true);
				plot.movimiento(60, 0, false);
				plot.finLetra();
				break;
			case "0":
				//caracter = "0";
				plot.movimiento(0, 100, true);
				plot.movimiento(100, 100, true);
				plot.movimiento(100, 0, true);
				plot.movimiento(0, 0, true);						
				plot.movimiento(100, 100, true);
				plot.movimiento(110, 0, false);
				plot.finLetra();
				break;
			default:
				break;
		} // fin estructura switch
	} // fin m�todo imprimir
	
	// mueve el motor del papel para adelante para soltarlo
	public void adelante(){
		plot.soltarPapel();
	}
	
	// mueve el motor del papel para atr�s para recogerlo
	public void atras(){
		plot.recogerPapel();
	}

} // fin de la clase BibliotecaMovimientosLetras