package morseprinter;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import lejos.utility.PilotProps;
import lejos.hardware.lcd.LCD;
import lejos.robotics.navigation.DifferentialPilot;
/*
 * Clase que controla los movimientos de la impresora
 * Recibe los par�metros de los movimientos necesarios para dibujar cada parte de una letra
 * */

public class PlotStep {

	// Atributos
	RegulatedMotor motorZ;	// motor para subir o bajar el lapicero (movimiento en el eje z)
	RegulatedMotor motorY;	// motor para mover el lapicero (movimiento en el eje y)
	RegulatedMotor motorX;	// motor para mover el papel (movimiento en el eje x)
	Boolean zPrevPos; 		// valor anterior en z
	int xPrevPos;			// valor anterior en x
	int yPrevPos;			// valor anterior en y

	// Constructor
	public PlotStep() throws Exception{
		zPrevPos = false;
		xPrevPos = 0;
		yPrevPos = 0;
		
		PilotProps pp = new PilotProps();
        pp.loadPersistentValues();
        float wheelDiameter = Float.parseFloat(pp.getProperty(PilotProps.KEY_WHEELDIAMETER, "4.0"));
        float trackWidth = Float.parseFloat(pp.getProperty(PilotProps.KEY_TRACKWIDTH, "18.0"));
         
        /*System.out.println("Wheel diameter is " + wheelDiameter);
        System.out.println("Track width is " +trackWidth);*/
         
        motorX = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_LEFTMOTOR, "C"));
        motorY = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_RIGHTMOTOR, "A"));
        motorZ = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_RIGHTMOTOR, "B"));
 
	} // fin del constructor
	
	// gira los motores X, Y y Z "x", "y" y "z" grados respectivamente
	public void movimiento(int x, int y, boolean z) throws Exception{
		
		motorX.resetTachoCount();
        motorY.resetTachoCount();
        motorZ.resetTachoCount(); 
        
        posZ(z); // solicitar el movimiento del motor Z
		posX(x); // solicitar el movimiento del motor X
		posY(y); // solicitar el movimiento del motor Y
		
		esperar(); // esperar a que los motores X y Y terminen de hacer el movimiento
		
	} // fin del m�todo movimiento
	
	// gira el motorZ "z" grados
	public void posZ(Boolean z){
		//false es arriba, true abajo
		if ((zPrevPos != z) ){
			motorZ.rotate(180);
			zPrevPos = z;
		}
	} // fin del m�todo posZ
	
	// gira el motorY "y" grados
	public void posY(int y){
		//gira desde coordenada 0 a 100... internamente en grados es de 0 a 292
		int yPosParametro = y*292/100;
		int yProxPos = yPosParametro-yPrevPos;
		yPrevPos = yPosParametro;
		motorY.rotate(yProxPos,true);
	} // fin del m�todo posY
	
	// gira el motorX "x" grados
	public void posX(int x){
		//gira desde coordenada 0 a 100... internamente en grados es de 0 a 292
		int xPosParametro = x*292/100;
		int xProxPos = xPosParametro-xPrevPos;
		xPrevPos = xPosParametro;
		motorX.rotate(xProxPos,true);
	} // fin del m�todo posX
	
	// Espera a que los motores X � Y terminen de hacer su movimiento
	public void esperar(){
		while(motorX.isMoving() || motorY.isMoving()){
			// esperar
		}
	} // fin m�todo esperar
	
	// reinicializar valores de ejes
	public void finLetra(){
		zPrevPos = false;
		xPrevPos = 0;
		yPrevPos = 0;
	} // fin m�todo finLetra
	
	// suelta el papel
	public void soltarPapel(){
		motorX.rotate(360,true);
	}
	
	//recoge el papel
	public void recogerPapel(){
		motorX.rotate(-360,true);		
	}

} // fin de la clase PlotStep
