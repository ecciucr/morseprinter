package morseprinter;
import lejos.hardware.Sound;
import java.io.File;

/*
 * Dice las letras, numeros y error
 * */
public class Comunicador {
	
	// Atributos
	private final int VOLUMEN = 100;									// volumen en el que se reproduce el sonido
	private final File soundAFile = new File("Sounds/Alphabet/A.wav");	// archivo wav con sonido letra A
	private final File soundBFile = new File("Sounds/Alphabet/B.wav");	// archivo wav con sonido letra B
	private final File soundCFile = new File("Sounds/Alphabet/C.wav");	// archivo wav con sonido letra C
	private final File soundDFile = new File("Sounds/Alphabet/D.wav");	// archivo wav con sonido letra D
	private final File soundEFile = new File("Sounds/Alphabet/E.wav");	// archivo wav con sonido letra E
	private final File soundFFile = new File("Sounds/Alphabet/F.wav");	// archivo wav con sonido letra F
	private final File soundGFile = new File("Sounds/Alphabet/G.wav");	// archivo wav con sonido letra G
	private final File soundHFile = new File("Sounds/Alphabet/H.wav");	// archivo wav con sonido letra H
	private final File soundIFile = new File("Sounds/Alphabet/I.wav");	// archivo wav con sonido letra I
	private final File soundJFile = new File("Sounds/Alphabet/J.wav");	// archivo wav con sonido letra J
	private final File soundKFile = new File("Sounds/Alphabet/K.wav");	// archivo wav con sonido letra K
	private final File soundLFile = new File("Sounds/Alphabet/L.wav");	// archivo wav con sonido letra L
	private final File soundMFile = new File("Sounds/Alphabet/M.wav");	// archivo wav con sonido letra M
	private final File soundNFile = new File("Sounds/Alphabet/N.wav");	// archivo wav con sonido letra N
	private final File sound�File = new File("Sounds/Alphabet/N.wav");	// archivo wav con sonido letra �
	private final File soundOFile = new File("Sounds/Alphabet/O.wav");	// archivo wav con sonido letra O
	private final File soundPFile = new File("Sounds/Alphabet/P.wav");	// archivo wav con sonido letra P
	private final File soundQFile = new File("Sounds/Alphabet/Q.wav");	// archivo wav con sonido letra Q
	private final File soundRFile = new File("Sounds/Alphabet/R.wav");	// archivo wav con sonido letra R
	private final File soundSFile = new File("Sounds/Alphabet/S.wav");	// archivo wav con sonido letra S
	private final File soundTFile = new File("Sounds/Alphabet/T.wav");	// archivo wav con sonido letra T
	private final File soundUFile = new File("Sounds/Alphabet/U.wav");	// archivo wav con sonido letra U
	private final File soundVFile = new File("Sounds/Alphabet/V.wav");	// archivo wav con sonido letra V
	private final File soundWFile = new File("Sounds/Alphabet/W.wav");	// archivo wav con sonido letra W
	private final File soundXFile = new File("Sounds/Alphabet/X.wav");	// archivo wav con sonido letra X
	private final File soundYFile = new File("Sounds/Alphabet/Y.wav");	// archivo wav con sonido letra Y
	private final File soundZFile = new File("Sounds/Alphabet/Z.wav");	// archivo wav con sonido letra Z
	private final File sound0File = new File("Sounds/Numbers/0.wav");	// archivo wav con sonido n�mero 0
	private final File sound1File = new File("Sounds/Numbers/1.wav");	// archivo wav con sonido n�mero 1
	private final File sound2File = new File("Sounds/Numbers/2.wav");	// archivo wav con sonido n�mero 2
	private final File sound3File = new File("Sounds/Numbers/3.wav");	// archivo wav con sonido n�mero 3
	private final File sound4File = new File("Sounds/Numbers/4.wav");	// archivo wav con sonido n�mero 4
	private final File sound5File = new File("Sounds/Numbers/5.wav");	// archivo wav con sonido n�mero 5
	private final File sound6File = new File("Sounds/Numbers/6.wav");	// archivo wav con sonido n�mero 6
	private final File sound7File = new File("Sounds/Numbers/7.wav");	// archivo wav con sonido n�mero 7
	private final File sound8File = new File("Sounds/Numbers/8.wav");	// archivo wav con sonido n�mero 8
	private final File sound9File = new File("Sounds/Numbers/9.wav");	// archivo wav con sonido n�mero 9
	private final File soundNopeFile	= new File("Sounds/Common/Nope.wav");		// archivo wav con sonido de palabra Nope
	private final File soundImSorryFile = new File("Sounds/Common/Im_sorry.wav");	// archivo wav con sonido de palabra I'm Sorry
	private final File soundButFile		= new File("Sounds/Common/BUT.wav");		// archivo wav con sonido de palabra But
	private final File soundErrorFile	= new File("Sounds/Common/Error.wav");		// archivo wav con sonido de palabra Error
	
	
	// Costructor
	public Comunicador(){
	}// fin del contructor
	
	// METODOS
	
	public void error(){
	 	Sound.buzz();
	}
	
	public void ok(){
		Sound.beepSequenceUp();			
	}
	
	// reproduce el sonido del archivo del primer par�metro al volumen indicado en el segundo par�metro
	public void reproducirArchivoSonido(File archivo, int volumen){
		Sound.playSample(archivo, volumen);		
	} // fin del m�todo reproducirArchivo
	
	// reproduce el sonido del caracter del par�metro
	public void decirCaracter(String caracter){
		switch(caracter){
			case "A": //caracter = "A";
				this.reproducirArchivoSonido(soundAFile, VOLUMEN);
				break;
			case "B": //caracter = "B";
				this.reproducirArchivoSonido(soundBFile, VOLUMEN);
				break;
			case "C": //caracter = "C";
				this.reproducirArchivoSonido(soundCFile, VOLUMEN);
				break;
			case "D": //caracter = "D";
				this.reproducirArchivoSonido(soundDFile, VOLUMEN);
				break;
			case "E": //caracter = "E";
				this.reproducirArchivoSonido(soundEFile, VOLUMEN);
				break;
			case "F": //caracter = "F";
				this.reproducirArchivoSonido(soundFFile, VOLUMEN);
				break;
			case "G": //caracter = "G";
				this.reproducirArchivoSonido(soundGFile, VOLUMEN);
				break;
			case "H": //caracter = "H";
				this.reproducirArchivoSonido(soundHFile, VOLUMEN);
				break;
			case "I": //caracter = "I";
				this.reproducirArchivoSonido(soundIFile, VOLUMEN);
				break;
			case "J": //caracter = "J";
				this.reproducirArchivoSonido(soundJFile, VOLUMEN);
				break;
			case "K": //caracter = "K";
				this.reproducirArchivoSonido(soundKFile, VOLUMEN);
				break;
			case "L": //caracter = "L";
				this.reproducirArchivoSonido(soundLFile, VOLUMEN);
				break;
			case "M": //caracter = "M";
				this.reproducirArchivoSonido(soundMFile, VOLUMEN);
				break;
			case "N": //caracter = "N";
				this.reproducirArchivoSonido(soundNFile, VOLUMEN);
				break;
			case "�": //caracter = "�";
				this.reproducirArchivoSonido(sound�File, VOLUMEN);
				break;
			case "O": //caracter = "O";
				this.reproducirArchivoSonido(soundOFile, VOLUMEN);
				break;
			case "P": //caracter = "P";
				this.reproducirArchivoSonido(soundPFile, VOLUMEN);
				break;
			case "Q": //caracter = "Q";
				this.reproducirArchivoSonido(soundQFile, VOLUMEN);
				break;
			case "R": //caracter = "R";
				this.reproducirArchivoSonido(soundRFile, VOLUMEN);
				break;
			case "S": //caracter = "S";
				this.reproducirArchivoSonido(soundSFile, VOLUMEN);
				break;
			case "T": //caracter = "T";
				this.reproducirArchivoSonido(soundTFile, VOLUMEN);
				break;
			case "U": //caracter = "U";
				this.reproducirArchivoSonido(soundUFile, VOLUMEN);
				break;
			case "V": //caracter = "V";
				this.reproducirArchivoSonido(soundVFile, VOLUMEN);
				break;
			case "W": //caracter = "W";
				this.reproducirArchivoSonido(soundWFile, VOLUMEN);
				break;
			case "X": //caracter = "X";
				this.reproducirArchivoSonido(soundXFile, VOLUMEN);
				break;
			case "Y": //caracter = "Y";
				this.reproducirArchivoSonido(soundYFile, VOLUMEN);
				break;
			case "Z": //caracter = "Z";
				this.reproducirArchivoSonido(soundZFile, VOLUMEN);
				break;
			case "1": //caracter = "1";
				this.reproducirArchivoSonido(sound1File, VOLUMEN);
				break;
			case "2": //caracter = "2";
				this.reproducirArchivoSonido(sound2File, VOLUMEN);
				break;
			case "3": //caracter = "3";
				this.reproducirArchivoSonido(sound3File, VOLUMEN);
				break;
			case "4": //caracter = "4";
				this.reproducirArchivoSonido(sound4File, VOLUMEN);
				break;
			case "5": //caracter = "5";
				this.reproducirArchivoSonido(sound5File, VOLUMEN);
				break;
			case "6": //caracter = "6";
				this.reproducirArchivoSonido(sound6File, VOLUMEN);
				break;
			case "7": //caracter = "7";
				this.reproducirArchivoSonido(sound7File, VOLUMEN);
				break;
			case "8": //caracter = "8";
				this.reproducirArchivoSonido(sound8File, VOLUMEN);
				break;
			case "9": //caracter = "9";
				this.reproducirArchivoSonido(sound9File, VOLUMEN);
				break;
			case "0": //caracter = "0";
				this.reproducirArchivoSonido(sound0File, VOLUMEN);
				break;
			default:
				break;
		} // fin estructura switch	
		
	} // fin del m�todo decirCaracter
	
	// reproduce el sonido de la palabra error
	public void decirError(){
		Sound.playSample(soundErrorFile, VOLUMEN);
	} // fin del m�todo decirError
	
	// decir la letra A
	public void decirLetraA(){
		Sound.playSample(soundAFile, VOLUMEN);
	} // fin del m�todo decirLetraA
	
	// decir la letra B
	public void decirLetraB(){
		Sound.playSample(soundBFile, VOLUMEN);
	} // fin del m�todo decirLetraB
	
	// decir la letra C
	public void decirLetraC(){
		Sound.playSample(soundCFile, VOLUMEN);
	} // fin del m�todo decirLetraC
	
	// decir la letra D
	public void decirLetraD(){
		Sound.playSample(soundDFile, VOLUMEN);
	} // fin del m�todo decirLetraD
	
	// decir la letra E
	public void decirLetraE(){
		Sound.playSample(soundEFile, VOLUMEN);
	} // fin del m�todo decirLetraE
	
	// decir la letra F
	public void decirLetraF(){
		Sound.playSample(soundFFile, VOLUMEN);
	} // fin del m�todo decirLetraF
	
	// decir la letra G
	public void decirLetraG(){
		Sound.playSample(soundGFile, VOLUMEN);
	} // fin del m�todo decirLetraG
	
	// decir la letra H
	public void decirLetraH(){
		Sound.playSample(soundHFile, VOLUMEN);
	} // fin del m�todo decirLetraH
	
	// decir la letra I
	public void decirLetraI(){
		Sound.playSample(soundIFile, VOLUMEN);
	} // fin del m�todo decirLetraI
	
	// decir la letra J
	public void decirLetraJ(){
		Sound.playSample(soundJFile, VOLUMEN);
	} // fin del m�todo decirLetraJ
	
	// decir la letra K
	public void decirLetraK(){
		Sound.playSample(soundKFile, VOLUMEN);
	} // fin del m�todo decirLetraK
	
	// decir la letra L
	public void decirLetraL(){
		Sound.playSample(soundLFile, VOLUMEN);
	} // fin del m�todo decirLetraL
	
	// decir la letra M
	public void decirLetraM(){
		Sound.playSample(soundMFile, VOLUMEN);
	} // fin del m�todo decirLetraM
	
	// decir la letra N
	public void decirLetraN(){
		Sound.playSample(soundNFile, VOLUMEN);
	} // fin del m�todo decirLetraN
	
	// decir la letra �
	public void decirLetra�(){
		Sound.playSample(sound�File, VOLUMEN);
	} // fin del m�todo decirLetra�
	
	// decir la letra O
	public void decirLetraO(){
		Sound.playSample(soundOFile, VOLUMEN);
	} // fin del m�todo decirLetraO
	
	// decir la letra P
	public void decirLetraP(){
		Sound.playSample(soundPFile, VOLUMEN);
	} // fin del m�todo decirLetraP
	
	// decir la letra Q
	public void decirLetraQ(){
		Sound.playSample(soundQFile, VOLUMEN);
	} // fin del m�todo decirLetraQ
	
	// decir la letra R
	public void decirLetraR(){
		Sound.playSample(soundRFile, VOLUMEN);
	} // fin del m�todo decirLetraR
	
	// decir la letra S
	public void decirLetraS(){
		Sound.playSample(soundSFile, VOLUMEN);
	} // fin del m�todo decirLetraS
	
	// decir la letra T
	public void decirLetraT(){
		Sound.playSample(soundTFile, VOLUMEN);
	} // fin del m�todo decirLetraT
	
	// decir la letra U
	public void decirLetraU(){
		Sound.playSample(soundUFile, VOLUMEN);
	} // fin del m�todo decirLetraU
	
	// decir la letra V
	public void decirLetraV(){
		Sound.playSample(soundVFile, VOLUMEN);
	} // fin del m�todo decirLetraV
	
	// decir la letra W
	public void decirLetraW(){
		Sound.playSample(soundWFile, VOLUMEN);
	} // fin del m�todo decirLetraW
	
	// decir la letra X
	public void decirLetraX(){
		Sound.playSample(soundXFile, VOLUMEN);
	} // fin del m�todo decirLetraX
	
	// decir la letra Y
	public void decirLetraY(){
		Sound.playSample(soundYFile, VOLUMEN);
	} // fin del m�todo decirLetraY
	
	// decir la letra Z
	public void decirLetraZ(){
		Sound.playSample(soundZFile, VOLUMEN);
	} // fin del m�todo decirLetraZ
	
	// decir el n�mero 0
	public void decirNumero0(){
		Sound.playSample(sound0File, VOLUMEN);
	} // fin del m�todo decirNumero0
	
	// decir el n�mero 1
	public void decirNumero1(){
		Sound.playSample(sound1File, VOLUMEN);
	} // fin del m�todo decirNumero1
	
	// decir el n�mero 2
	public void decirNumero2(){
		Sound.playSample(sound2File, VOLUMEN);
	} // fin del m�todo decirNumero2
	
	// decir el n�mero 3
	public void decirNumero3(){
		Sound.playSample(sound3File, VOLUMEN);
	} // fin del m�todo decirNumero3
	
	// decir el n�mero 4
	public void decirNumero4(){
		Sound.playSample(sound4File, VOLUMEN);
	} // fin del m�todo decirNumero4
	
	// decir el n�mero 5
	public void decirNumero5(){
		Sound.playSample(sound5File, VOLUMEN);
	} // fin del m�todo decirNumero5
	
	// decir el n�mero 6
	public void decirNumero6(){
		Sound.playSample(sound6File, VOLUMEN);
	} // fin del m�todo decirNumero6
	
	// decir el n�mero 7
	public void decirNumero7(){
		Sound.playSample(sound7File, VOLUMEN);
	} // fin del m�todo decirNumero7
	
	// decir el n�mero 8
	public void decirNumero8(){
		Sound.playSample(sound8File, VOLUMEN);
	} // fin del m�todo decirNumero8
	
	// decir el n�mero 9
	public void decirNumero9(){
		Sound.playSample(sound9File, VOLUMEN);
	} // fin del m�todo decirNumero9

} // fin de la clase Comunicador
