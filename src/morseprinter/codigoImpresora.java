package morseprinter;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import lejos.utility.PilotProps;
import lejos.hardware.lcd.LCD;
import lejos.robotics.navigation.DifferentialPilot;



public class codigoImpresora
{
	static RegulatedMotor motorZ;
	static RegulatedMotor motorY;
	static RegulatedMotor motorX;
	static Boolean zPrevPos =false;
	static int xPrevPos =0;
	static int yPrevPos =0;


		
	public static void main(String[] args ) throws Exception
	{
		introMessage();
		PilotProps pp = new PilotProps();
        pp.loadPersistentValues();
        float wheelDiameter = Float.parseFloat(pp.getProperty(PilotProps.KEY_WHEELDIAMETER, "4.0"));
        float trackWidth = Float.parseFloat(pp.getProperty(PilotProps.KEY_TRACKWIDTH, "18.0"));
         
        System.out.println("Wheel diameter is " + wheelDiameter);
        System.out.println("Track width is " +trackWidth);
         
        motorX = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_LEFTMOTOR, "C"));
        motorY = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_RIGHTMOTOR, "A"));
        motorZ = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_RIGHTMOTOR, "B"));

        motorX.resetTachoCount();
        motorY.resetTachoCount();
        motorZ.resetTachoCount();
        
        //DifferentialPilot robot = new DifferentialPilot(wheelDiameter,trackWidth,leftMotor,rightMotor);
          
        //Button.waitForAnyPress();
        // velocidad de 100 
        // Wait for user to press ENTER
        
        //rightMotor.setAcceleration(400);
        //leftMotor.setAcceleration(400);
        
        for(int i=0;i<4;i++){
	        
	        //robot.setTravelSpeed(200); // cm/sec
	        //robot.forward();
        	/* ************ */
        	/* Pruebas de Z */
        	/* ************ */
        	/*
	        System.out.println("Asegurece que el motor en Z este arriba");
	        Button.waitForAnyPress();
	        posZ(true);
	        System.out.println("bajo");
	        Button.waitForAnyPress();
	        posZ(false);
	        System.out.println("subio");
	        Button.waitForAnyPress();
	        posZ(false);
	        System.out.println("se mantuvo arriba");
	        Button.waitForAnyPress();
	        posZ(true);
	        System.out.println("bajo");
	        posZ(false);
	        Button.waitForAnyPress();
	        System.out.println("subio");
	        Button.waitForAnyPress();
	        posZ(false);
	        System.out.println("Se mantuvo arriba ");
	        Button.waitForAnyPress();
	        System.out.println("Termino ");
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        //*/
        	
        	/* ************ */
        	/* Pruebas de Y */
        	/* ************ */
        	//*
	        System.out.println("Asegurece que el motor en Y lo mas lejos de su motor");
	        System.out.println("");
	        System.out.println("");
	        

	        
	        
	        
	        
	        posZ(false);
	        posX(0);
	        posY(75);
	        System.out.println("0,100");
	        Button.waitForAnyPress();
	        posZ(true);
	        posX(75);
	        posY(0);
	        System.out.println("100,0");
	        Button.waitForAnyPress();
	        posZ(true);
	        posX(150);
	        posY(75);
	        System.out.println("200,100");
	        Button.waitForAnyPress();
	        posZ(false);
	        posX(200);
	        posY(0);
	        System.out.println("50,0");
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        

	        posX(50);
	        posY(0);
	        System.out.println("100,100");
	        Button.waitForAnyPress();
	        posX(100);
	        posY(100);
	        Button.waitForAnyPress();
	        posZ(false);
	        System.out.println("subio");
	        System.out.println("TErmino");
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        posY(25);
	        System.out.println("y 25%");
	        Button.waitForAnyPress();
	        posY(100);
	        System.out.println("y 100%");
	        Button.waitForAnyPress();
	        posY(0);
	        System.out.println("y 0%");
	        Button.waitForAnyPress();
	        posZ(false);
	        System.out.println("subio");
	        System.out.println("Termino ");
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        Button.waitForAnyPress();
	        //*/
	        
	        
	        Button.waitForAnyPress();
	        
	        
	        motorZ.rotate(180);
	        Button.waitForAnyPress();
	        motorZ.rotate(180);
	        Button.waitForAnyPress();
	        motorZ.rotate(180);
	        Button.waitForAnyPress();
	        motorZ.rotate(180);
	        Button.waitForAnyPress();
	        
	        motorX.rotate(180);
	        Button.waitForAnyPress();
	        motorX.rotate(-180);
	        motorX.rotate(180);
	        Button.waitForAnyPress();
	        motorX.rotate(-180);
	        motorX.rotate(180);
	        Button.waitForAnyPress();
	        motorX.rotate(-180);
	        
	        
	        
	        /*
	        leftMotor.setSpeed(400);
	        rightMotor.setSpeed(400);
			leftMotor.forward();
			rightMotor.forward();
			Delay.msDelay(10000);
			leftMotor.stop(true);
			rightMotor.stop(true);
	        Delay.msDelay(2500);
	        System.out.println("Stopping");
	        //robot.stop();
	        System.out.println("Stopped");
	        Delay.msDelay(1000);
	        // velocidad de 25
	    	System.out.println("giro 80");
			//Button.waitForAnyPress();
	    	rightMotor.setSpeed(78);
	    	rightMotor.forward();
			System.out.println("Going forwards");
			Delay.msDelay(5000);
			System.out.println("Stopping");
			rightMotor.stop();
			System.out.println("Stopped");
			
			
			*/
        }
		
		/*
    	PilotProps pp = new PilotProps();
    	leftMotor = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_LEFTMOTOR, "B"));
    	
    	// velocidad de 25
    	System.out.println("velocidad 25");
		Button.waitForAnyPress();
		leftMotor.setSpeed(25);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 50
    	System.out.println("velocidad 50");
		Button.waitForAnyPress();
		leftMotor.setSpeed(50);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 75
    	System.out.println("velocidad 75");
		Button.waitForAnyPress();
		leftMotor.setSpeed(75);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
				
		// velocidad de 100
    	System.out.println("velocidad 100");
		Button.waitForAnyPress();
		leftMotor.setSpeed(100);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 125
    	System.out.println("velocidad 125");
		Button.waitForAnyPress();
		leftMotor.setSpeed(125);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 150
    	System.out.println("velocidad 150");
		Button.waitForAnyPress();
		leftMotor.setSpeed(150);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 200
    	System.out.println("velocidad 200");
		Button.waitForAnyPress();
		leftMotor.setSpeed(200);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 250
    	System.out.println("velocidad 250");
		Button.waitForAnyPress();
		leftMotor.setSpeed(250);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 300
    	System.out.println("velocidad 300");
		Button.waitForAnyPress();
		leftMotor.setSpeed(300);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		// velocidad de 350
    	System.out.println("velocidad 350");
		Button.waitForAnyPress();
		leftMotor.setSpeed(350);
		leftMotor.forward();
		System.out.println("Going forwards");
		Delay.msDelay(5000);
		System.out.println("Stopping");
		leftMotor.stop();
		System.out.println("Stopped");
		
		System.out.println("Waiting for button press");
		// Exit after any button is pressed
		Button.waitForAnyPress();
	*/
	}
	public static void posZ(Boolean z){
		//false es arriba, true abajo
		if ((zPrevPos != z) ){
			motorZ.rotate(180);
			zPrevPos = z;
		}
	}
	
	public static void posY(int y){
		//gira desde coordenada 0 a 100... internamente en grados es de 0 a 292
		//if(y>=0 && y<=100){
			int yPosParametro = y*292/100;
			int yProxPos = yPosParametro-yPrevPos;
			yPrevPos = yPosParametro;
			motorY.rotate(yProxPos,true);
		//}
		//else
	       // System.out.println("Ingrese pos en y valida(0 a 100)");

	}
	public static void posX(int x){
		//gira desde coordenada 0 a 100... internamente en grados es de 0 a 292
		//if(x>=0 && x<=100){
			int xPosParametro = x*292/100;
			int xProxPos = xPosParametro-xPrevPos;
			xPrevPos = xPosParametro;
			motorX.rotate(xProxPos,true);
		//}
		//else
	     //   System.out.println("Ingrese pos en x valida(0 a 100)");

	}
	
	
   
	public static void introMessage() {
		
		GraphicsLCD g = LocalEV3.get().getGraphicsLCD();
		g.drawString("Odometria", 5, 0, 0);
		g.setFont(Font.getSmallFont());
		g.drawString("Plug motors into ports B and ", 2, 70, 0);
		g.drawString("C and press enter. ", 2, 80, 0);
		  
		// Quit GUI button:
		g.setFont(Font.getSmallFont()); // can also get specific size using Font.getFont()
		int y_quit = 100;
		int width_quit = 45;
		int height_quit = width_quit/2;
		int arc_diam = 6;
		g.drawString("QUIT", 9, y_quit+7, 0);
		g.drawLine(0, y_quit,  45, y_quit); // top line
		g.drawLine(0, y_quit,  0, y_quit+height_quit-arc_diam/2); // left line
		g.drawLine(width_quit, y_quit,  width_quit, y_quit+height_quit/2); // right line
		g.drawLine(0+arc_diam/2, y_quit+height_quit,  width_quit-10, y_quit+height_quit); // bottom line
		g.drawLine(width_quit-10, y_quit+height_quit, width_quit, y_quit+height_quit/2); // diagonal
		g.drawArc(0, y_quit+height_quit-arc_diam, arc_diam, arc_diam, 180, 90);
		
		// Enter GUI button:
		g.fillRect(width_quit+10, y_quit, height_quit, height_quit);
		g.drawString("GO", width_quit+15, y_quit+7, 0,true);
		
		Button.waitForAnyPress();
		if(Button.ESCAPE.isDown()) System.exit(0);
		g.clear();
		
	}
	
	
	
}