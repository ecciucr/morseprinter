package morseprinter;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.SensorMode;
import lejos.utility.Stopwatch;

public class Principal {
	
	public static void introMessage() {

		GraphicsLCD g = LocalEV3.get().getGraphicsLCD();
		g.drawString("Morse Code Printer", 5, 0, 0);
		  
	  	// Quit GUI button:
		//g.setFont(Font.getSmallFont()); // can also get specific size using Font.getFont() 
		int y_quit = 100;
		int width_quit = 45;
		int height_quit = width_quit/2;
		int arc_diam = 6;
		g.drawString("QUIT", 9, y_quit+7, 0);
		g.drawLine(0, y_quit,  45, y_quit); // top line
		g.drawLine(0, y_quit,  0, y_quit+height_quit-arc_diam/2); // left line
		g.drawLine(width_quit, y_quit,  width_quit, y_quit+height_quit/2); // right line
		g.drawLine(0+arc_diam/2, y_quit+height_quit,  width_quit-10, y_quit+height_quit); // bottom line
		g.drawLine(width_quit-10, y_quit+height_quit, width_quit, y_quit+height_quit/2); // diagonal
		g.drawArc(0, y_quit+height_quit-arc_diam, arc_diam, arc_diam, 180, 90);
		
		// Enter GUI button:
		g.fillRect(width_quit+10, y_quit, height_quit, height_quit);
		g.drawString("GO", width_quit+15, y_quit+7, 0,true);
		
		Button.waitForAnyPress();
		if(Button.ESCAPE.isDown()) System.exit(0);
		g.clear();
}
	
	// M�todo Principal
	public static void main(String args[]) throws Exception{
		
		introMessage();
		
		// Atributos
		double umbralLuz = 0.02;					// m�nimo de luz para verificar que el sensor est� cubierto
		boolean bloqueo = true; 					// bandera para bloquear y desbloquear el conteo de tiempo de presionado del sensor de tacto
		Decodificador dec = new Decodificador();	// tradutor de morse a letras
		Stopwatch reloj = new Stopwatch(); 			// contador de tiempo
		String morse = ""; 							// hilera para acumular el c�digo morse que el usuario introduce 
		String caracter = ""; 						// hilera para almacenar el caracter traducido
		GraphicsLCD g = LocalEV3.get().getGraphicsLCD(); // pantalla
		long tiempo1 = 0;							// tiempo en instante 1 
		long tiempo2 = 0;							// tiempo en instante 2
		long difTiempo = 0; 						// diferencia de tiempos (tiempo2 - tiempo1)
		int minTiempoRaya = 151;					// m�nimo de tiempo en milisegundos para considerar un pulso como raya
		Comunicador voz = new Comunicador();    	// reproduce el sonido de los caracteres
		BibliotecaMovimientosLetras bib = new BibliotecaMovimientosLetras();	// imprime los caracteres
		boolean bloqueoError = true;				// bandera para que se reproduzca el sonido de error solo una vez
		
		// inicializar el sensor de luz
		EV3ColorSensor sensorLuz = new EV3ColorSensor(SensorPort.S3);
		SensorMode ambienteLuz = sensorLuz.getAmbientMode();
		float[] sampleLuz = new float[ambienteLuz.sampleSize()];
		
		// inicializar el sensor de tacto
		EV3TouchSensor sensorTacto = new EV3TouchSensor(SensorPort.S4);
		SensorMode tacto = sensorTacto.getTouchMode();
		float[] sampleTacto = new float[tacto.sampleSize()];
		
		g.drawString("Tape el sensor de luz!", 10, 0, 0);
		Button.waitForAnyPress();
		g.clear();
		
		// mientras no se presione el bot�n de escape
		while(Button.ESCAPE.isUp()){
			
			// leer el sensor de luz
			ambienteLuz.fetchSample(sampleLuz, 0);
			
			if(sampleLuz[0] <= umbralLuz){ // si el sensor de luz est� cubierto
				
				// escuchar el sensor de tacto
				tacto.fetchSample(sampleTacto, 0); // leer sensor de tacto
				
				if(sampleTacto[0] == 0.0){ // si el sensor de tacto est� liberado
					tiempo1 = reloj.elapsed();
					
					if(bloqueo == false){
						// verificar el tiempo de presionado del bot�n 
						// (dependiendo del tiempo que se mantenga 
						// presionado el sensor se interpreta como una raya o como un punto)
						if(difTiempo >= minTiempoRaya){ // interpretar como una raya
							morse += "-";
						}else{ // interpretar como un punto
							morse += ".";
						}
						bloqueo=true;
					}
										
				}else{ // el sensor de tacto est� presionado
					tiempo2 = reloj.elapsed();
					difTiempo = tiempo2-tiempo1;
					bloqueo = false;
					bloqueoError = false;
				}
								
			}else{ // el sensor de luz est� descubierto
				
				// procesar el c�digo morse
				caracter = dec.traducir(morse); // truducir el c�digo morse
				
				if(caracter != ""){
					voz.ok(); // reproducir sonido de suceso
					// imprimir el caracter en el LCD del ladrillo
					g.clear();
					g.drawString(caracter, 5, 0, 0);
					// imprimir el caracter en el papel
					bib.imprimir(caracter);
					bloqueoError = true;
				}else{ // error
					if(!bloqueoError){
						voz.error(); // reproducir el sonido de error 
						bloqueoError = true; // bloquear el sonido de error ya que se debe reproducir solo una vez
					}
					g.clear();
					g.drawString("Error de entrada!", 5, 0, 0);
				}
				
				g.clear();		// limpiar el LCD
				morse="";  		// borrar el codigo morse para volver a usar el acumulador
				bloqueo=true;	// reiniciar la bandera de bloqueo
			}
			
			// soltar papel si se presiona el bot�n derecho
			if(Button.RIGHT.isDown()){
				bib.adelante();				
			}
			
			// recger papel si se presiona el bot�n izquierdo
			if(Button.LEFT.isDown()){
				bib.atras();				
			}
			
		} // fin while del bot�n de escape
				
		// cerrar los sensores
		sensorLuz.close();
		sensorTacto.close();
		
	} // fin del m�todo main

} // fin de la clase principal
