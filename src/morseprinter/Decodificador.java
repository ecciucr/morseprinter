package morseprinter;

/*
 * Interpreta una hilera de caracteres en c�digo morse y retorna el caracter correspondiente
 */
public class Decodificador {

	// Constantes
	private static final String LETRA_A = ".-";		// c�digo morse de la letra A
	private static final String LETRA_B = "-...";	// c�digo morse de la letra B	
	private static final String LETRA_C = "-.-.";	// c�digo morse de la letra C
	private static final String LETRA_D = "-..";	// c�digo morse de la letra D
	private static final String LETRA_E = ".";		// c�digo morse de la letra E
	private static final String LETRA_F = "..-.";	// c�digo morse de la letra F
	private static final String LETRA_G = "--.";	// c�digo morse de la letra G
	private static final String LETRA_H = "....";	// c�digo morse de la letra H
	private static final String LETRA_I = "..";		// c�digo morse de la letra I
	private static final String LETRA_J = ".---";	// c�digo morse de la letra J
	private static final String LETRA_K = "-.-";	// c�digo morse de la letra K
	private static final String LETRA_L = ".-..";	// c�digo morse de la letra L
	private static final String LETRA_M = "--";		// c�digo morse de la letra M
	private static final String LETRA_N = "-.";		// c�digo morse de la letra N
	private static final String LETRA_� = "--.--";	// c�digo morse de la letra �
	private static final String LETRA_O = "---";	// c�digo morse de la letra O
	private static final String LETRA_P = ".--.";	// c�digo morse de la letra P
	private static final String LETRA_Q = "--.-";	// c�digo morse de la letra Q
	private static final String LETRA_R = ".-.";	// c�digo morse de la letra R
	private static final String LETRA_S = "...";	// c�digo morse de la letra S
	private static final String LETRA_T = "-";		// c�digo morse de la letra T
	private static final String LETRA_U = "..-";	// c�digo morse de la letra U
	private static final String LETRA_V = "...-";	// c�digo morse de la letra V
	private static final String LETRA_W = ".--";	// c�digo morse de la letra W
	private static final String LETRA_X = "-..-";	// c�digo morse de la letra X
	private static final String LETRA_Y = "-.--";	// c�digo morse de la letra Y
	private static final String LETRA_Z = "--..";	// c�digo morse de la letra Z
	private static final String NUM_1 	= ".----";	// c�digo morse del n�mero 1
	private static final String NUM_2 	= "..---";	// c�digo morse del n�mero 2
	private static final String NUM_3 	= "...--";	// c�digo morse del n�mero 3
	private static final String NUM_4 	= "....-";	// c�digo morse del n�mero 4
	private static final String NUM_5 	= ".....";	// c�digo morse del n�mero 5
	private static final String NUM_6 	= "-....";	// c�digo morse del n�mero 6
	private static final String NUM_7 	= "--...";	// c�digo morse del n�mero 7
	private static final String NUM_8 	= "---..";	// c�digo morse del n�mero 8
	private static final String NUM_9 	= "----.";	// c�digo morse del n�mero 9
	private static final String NUM_0 	= "-----";	// c�digo morse del n�mero 0
	private static final String SIMBOLO_PUNTO 			= ".-.-.-"; // c�digo morse del s�mbolo ;
	private static final String SIMBOLO_COMA 			= "--..--";	// c�digo morse del s�mbolo ,
	private static final String SIMBOLO_INTERROGACION	= "..--..";	// c�digo morse del s�mbolo ?
	private static final String SIMBOLO_COMILLAS		= ".-..-.";	// c�digo morse del s�mbolo "
	private static final String SIMBOLO_ADMIRACION		= "-.-.--"; // c�digo morse del s�mbolo !
	
	// Constructor
	public Decodificador(){
	} // fin del constructor
	
	// traduce uns hilera de caracteres en c�digo morse a su caracter correspondiente
	public String traducir(String codigoMorse){
		String caracter = "";
		
		switch(codigoMorse){
		case LETRA_A:
			caracter = "A";
			break;
		case LETRA_B:
			caracter = "B";
			break;
		case LETRA_C:
			caracter = "C";
			break;
		case LETRA_D:
			caracter = "D";
			break;
		case LETRA_E:
			caracter = "E";
			break;
		case LETRA_F:
			caracter = "F";
			break;
		case LETRA_G:
			caracter = "G";
			break;
		case LETRA_H:
			caracter = "H";
			break;
		case LETRA_I:
			caracter = "I";
			break;
		case LETRA_J:
			caracter = "J";
			break;
		case LETRA_K:
			caracter = "K";
			break;
		case LETRA_L:
			caracter = "L";
			break;
		case LETRA_M:
			caracter = "M";
			break;
		case LETRA_N:
			caracter = "N";
			break;
		case LETRA_�:
			caracter = "�";
			break;
		case LETRA_O:
			caracter = "O";
			break;
		case LETRA_P:
			caracter = "P";
			break;
		case LETRA_Q:
			caracter = "Q";
			break;
		case LETRA_R:
			caracter = "R";
			break;
		case LETRA_S:
			caracter = "S";
			break;
		case LETRA_T:
			caracter = "T";
			break;
		case LETRA_U:
			caracter = "U";
			break;
		case LETRA_V:
			caracter = "V";
			break;
		case LETRA_W:
			caracter = "W";
			break;
		case LETRA_X:
			caracter = "X";
			break;
		case LETRA_Y:
			caracter = "Y";
			break;
		case LETRA_Z:
			caracter = "Z";
			break;
		case NUM_1:
			caracter = "1";
			break;
		case NUM_2:
			caracter = "2";
			break;
		case NUM_3:
			caracter = "3";
			break;
		case NUM_4:
			caracter = "4";
			break;
		case NUM_5:
			caracter = "5";
			break;
		case NUM_6:
			caracter = "6";
			break;
		case NUM_7:
			caracter = "7";
			break;
		case NUM_8:
			caracter = "8";
			break;
		case NUM_9:
			caracter = "9";
			break;
		case NUM_0:
			caracter = "0";
			break;
		default:
			break;
		} // fin switch
		return caracter; // devolver el string del caracter
	} // fin de m�todo traducir
		
} // fin de la clase Decodificador
